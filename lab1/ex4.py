# Exercise 4
"""Input a string and pass it to the function. The function returns a tuple of
three values: a count of uppercase letters, a count of lowercase letters and a
count of numbers.

        char_types_count("asdf98CXX21grrr") → (3,8,4)
"""

def char_types_count(s):
    d={"UPPER_CASE":0, "LOWER_CASE":0, "DIGIT":0}
    for c in s:
        if c.isupper():
           d["UPPER_CASE"]+=1
        elif c.islower():
           d["LOWER_CASE"]+=1
        elif c.isdigit():
            d["DIGIT"]+=1
        else:
           pass
    print ("Original String : ", s)
    print ("No. of Upper case characters : ", d["UPPER_CASE"])
    print ("No. of Lower case Characters : ", d["LOWER_CASE"])
    print ("No. of digits : ", d["DIGIT"])

char_types_count("asdf98CXX21grrr")

