# Exercise 2
"""Input the comma-separated integers and transform them into a list of integers.
Return the "centered" average of an list of ints, which we'll say is the mean
average of the values, except ignoring the largest and smallest values in the
list. If there are multiple copies of the smallest value, ignore just one copy,
and likewise for the largest value. Use int division to produce the final
average. You may assume that the list is length 3 or more.

        centered_average([1, 2, 3, 4, 100]) → 3
        centered_average([1, 1, 5, 5, 10, 8, 7]) → 5
        centered_average([-10, -4, -2, -4, -2, 0]) → -3
"""

nums =[1, 2, 3, 4, 100]
def solution(input):

    average = 0

    minimum = min(input)
    maximum = max(input)

    input.pop(input.index(minimum))
    input.pop(input.index(maximum))

    average =  round(sum(input) / len(input))

    return average

print(solution(nums))