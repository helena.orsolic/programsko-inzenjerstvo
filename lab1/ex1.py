# Exercise 1
"""Input the comma-separated integers and transform them into a list of integers.
Print the number of even ints in the given list. Note: the % "mod" operator computes the remainder, e.g. 5 % 2 is 1.
Use a function for counting evens.

        count_evens([2, 1, 2, 3, 4]) → 3
        count_evens([2, 2, 0]) → 3
        count_evens([1, 3, 5]) → 0
"""

my_str = '1,2,3,4'

my_list = [int(item) for item in my_str.split(',') if item.isdigit()] #comma-sep int into list of ints
print(my_list)
even_count=0
for mod in my_list:
 if mod % 2 == 0:
        even_count += 1

print(even_count)