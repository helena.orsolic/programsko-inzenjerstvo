from query_handler_base import QueryHandlerBase
import requests
import json

class CoronaHandler(QueryHandlerBase):
    def can_process_query(self, query):
        if "covid" in query:
            return True
        return False

    def process(self, query):

        unos = query.split()

        try:
            result = self.call_api()
            for x in result:
                if x["Country"] == unos[1]:
                    self.ui.say(json.dumps(x, indent=4))
                    break
            self.ui.say("There is no such country! Try using capital first latter")
        except: 
            self.ui.say("Oh no! There was an error trying to contact Covid api")
            self.ui.say("Try something else!")

    def call_api(self):

        url = "https://vaccovid-coronavirus-vaccine-and-treatment-tracker.p.rapidapi.com/api/npm-covid-data/"

        headers = {
            "X-RapidAPI-Key": "b384cf3502msheb1483daec77279p1990c5jsn79b8117b4fa5",
            "X-RapidAPI-Host": "vaccovid-coronavirus-vaccine-and-treatment-tracker.p.rapidapi.com"
        }

        response = requests.request("GET", url, headers=headers)

        return json.loads(response.text)