from query_handler_base import QueryHandlerBase
import requests
import json

class GeniousHandler(QueryHandlerBase):
    def can_process_query(self, query):
        if "lyrics" in query:
            return True
        return False

    def process(self, query):

        unos = query.split()
        
        try:
            result = self.call_api(unos[1])
            for song in result['response']['hits']:
                self.ui.say(song['result']['full_title'])

        except: 
            self.ui.say("Oh no! There was an error trying to contact Genious api.")
            self.ui.say("Try something else!")

    def call_api(self, artist):
        url = "https://genius-song-lyrics1.p.rapidapi.com/search"

        querystring = {"q":artist,"per_page":"10","page":"1"}

        headers = {
            "X-RapidAPI-Key": "b384cf3502msheb1483daec77279p1990c5jsn79b8117b4fa5",
            "X-RapidAPI-Host": "genius-song-lyrics1.p.rapidapi.com"
        }

        response = requests.request("GET", url, headers=headers, params=querystring)

        return json.loads(response.text)