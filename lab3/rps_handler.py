from query_handler_base import QueryHandlerBase
import random

class RockPaperScissorsHandler(QueryHandlerBase):
    def can_process_query(self, query):
        if "rps" in query:
            return True
        return False
        
    def process(self, query):
        rps_params = query.split()
        user_action = rps_params[1]
        self.ui.say(f"You chose {user_action}")

        option = ["rock", "paper", "scissors"]
        computer_action = option[random.randint(0,2)]
        self.ui.say(f"I chose {computer_action}")

        if user_action == computer_action:
            self.ui.say(f"Both players selected {user_action}. It's a tie!")
        elif user_action == "rock":
            if computer_action == "scissors":
                self.ui.say("Rock smashes scissors! You win!")
            else:
                self.ui.say("Paper covers rock! You lose.")
        elif user_action == "paper":
            if computer_action == "rock":
                self.ui.say("Paper covers rock! You win!")
            else:
                self.ui.say("Scissors cuts paper! You lose.")
        elif user_action == "scissors":
            if computer_action == "paper":
                self.ui.say("Scissors cuts paper! You win!")
            else:
                self.ui.say("Rock smashes scissors! You lose.")