import json

class Employee:

    def __init__(self, name, title, age, office):
        self.name = name
        self.title = title
        self.age = age
        self.office = office

    def __str__(self):
        return f"{self.name} ({self.age}), {self.title} @ {self.office}"


class Company:

    def __init__(self, name):
        self.name = name
        self.employees = []

    def __str__(self):
        return f"{self.name} ({len(self.employees)} employees)"

    def employ(self, name, title, age, office):
        new_employee = Employee(name, title, age, office)
        self.employees.append(new_employee)

    def fire(self, name):
        for e in self.employees:
            if e.name == name:
                self.employees.remove(e)

    def load_from_json(self, path_to_json):
        with open(path_to_json, "r", encoding="utf-8") as f:
            employees = json.load(f)
        
        for e in employees:
            self.employees.append(Employee(e["Employee"], e["Job Title"], e["Age"], e["Office"]))
        pass
    
    def save_to_json(self, path_to_json):
        new = json.dumps(self, default=lambda o: o.__dict__, sort_keys=True, indent=4)
        with open(path_to_json, 'w', encoding='utf-8') as f:
            json.dump(new, f, ensure_ascii=False, indent=4)
        pass

    def print_employees(self):
        print(self.name)
        print("----------------")
        i=0
        for e in self.employees:
            i+=1
            print(f"{i}. {str(e)}")
        pass


def main():

    adidas = Company("Adidas")
    adidas.load_from_json("ex4-employees.json")
    print(adidas)
    adidas.print_employees()

    adidas.employ("Homer Simpson", "CEO", 44, "Lobby")
    adidas.employ("Marge Simpson", "CTO", 33, "Lobby")
    adidas.employ("Bart Simpson", "CEO", 44, "Lobby")
    adidas.employ("Lisa Simpson", "CTO", 33, "Lobby")
    print(adidas)
    adidas.print_employees()

    adidas.fire("Homer Simpson")
    adidas.fire("Marge Simpson")
    print(adidas)
    adidas.print_employees()

    adidas.save_to_json("ex6-employees.json")

if __name__ == "__main__":
    main()
