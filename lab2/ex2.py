"""
### Exercise 2

- Read section 7.2 from the tutorial.
- Read string methods documentation: 
  - https://docs.python.org/3/library/stdtypes.html#text-sequence-type-str
  - https://docs.python.org/3/library/stdtypes.html#string-methods
  - Give special focus to ` split() ` and ` strip() ` methods
- Write a simple program which loads the data from the `.csv` file (hint
  about [csv file format](https://www.howtogeek.com/348960/what-is-a-csv-file-and-how-do-i-open-it/) and writes it to different files
  - Read data from file `ex2-text.csv`
  - Parse it (split lines on ` , ` chars and store in variables)
  - Write data to files:
    - `ex2-employees.txt` in format `employee,job title`
    - `ex2-locations.txt` in format `employee,office`
- Save your solution as ` ex2.py ` and commit, along with created `.txt`
  files, all in the same commit.
  
  """

import csv

file = open('ex2-text.csv', mode = 'r')
csvreader = csv.reader(file, delimiter=',')

header = []
header = next(csvreader)
print(header)

rows = []
for row in csvreader:
        rows.append(row)

file.close()

wfile = open('ex2-employees.txt', mode = 'w')
csvwriter = csv.writer(wfile, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL )
csvwriter.writerow(['John Smith', 'Accounting'])
wfile.close()

wfile = open('ex2-locations.txt', mode = 'w')
csvwriter = csv.writer(wfile, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL )
csvwriter.writerow(['John Smith', '13-05'])
wfile.close()
