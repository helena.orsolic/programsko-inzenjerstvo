"""
- Read sections 9 - 9.4, with a special focus on 9.3
- Write a simple program that will:
- Use class `Employee`
- Load employees from `ex4-employees.json` file 
- Create a list `employees_list` 
- For each employee in `employees`, create an object of a type `Employee` and
  append it to the `employees_list`


"""
import json

class Employee:

    def __init__(self, name, title, age, office):
        self.name = name
        self.title = title
        self.age = age
        self.office = office

    def __str__(self):
        return f"{self.name} ({self.age}), {self.title} @ {self.office}"

with open("ex4-employees.json", "r", encoding="utf-8") as f:
    employees = json.load(f)

employees_list = []

for e in employees:
    employees_list.append(Employee(e["Employee"], e["Job Title"], e["Age"], e["Office"]))

for e in employees_list:
    print(e)