"""
 Write a simple program that will:
  - Load the data from `ex2-text.csv`
  - Parse the data and save it to a list of employees (var called
    `employees`)
  - Each item in the list is a dictionary with the employee's info
  - Print the created dictionary var
  - Example when printing the var `employees`

"""

import json

file = open("ex2-text.csv","r", encoding=None)

file.readline()

employees = []

for line in file: 
    Employee, jobtitle, age, office = line.split(",")
    list = {"Employee" : Employee, "Job Title" : jobtitle, "Age" : age, "Office" : office}
    employees.append(list)
    print(list)
    
file.close()

"""
with open('ex4-employees.json', 'w', encoding='utf-8') as f:
    json.dump(employees, f)
"""