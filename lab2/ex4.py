"""
Read section 7.2.2 (JSON)
- Modify the program ` ex3.py ` from the previous exercise. The modified
  program has to load and parse data to a dictionary, same as in ` ex3.py `.
  However, the modified program now has to save that data to disk in `json`

"""

import json

file = open("ex2-text.csv","r", encoding=None)

file.readline()

employees = []

for line in file: 
    Employee, jobtitle, age, office = line.split(",")
    dict = {"Employee" : Employee, "Job Title" : jobtitle, "Age" : age, "Office" : office}
    employees.append(dict)
    print(dict)
    
file.close()


with open('ex4-employees.json', 'w', encoding='utf-8') as f:
    json.dump(employees, f)
